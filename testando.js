// ////////////////////////
// //  Aula JS - Dia 1  //
// //////////////////////
// let array = "Samuel"


// //  Strings Methods
// let tamanho = array.length //length é um atributo que conta os elementos da string
// console.log("tamanho da string:",tamanho)
// let s = array.toUpperCase()
// console.log(s)
// s = array.toLowerCase()
// console.log(s)

// //  Array Propriedades
// let palavra = ['s','a','m','u','e','l']
// //  Array Methods
// let a = palavra.push("bello")
// console.log(a)
// ///////



const array1 = [5, 1, 8, 130, 44];

// const found = array1.find(k => k > 10);
function isBigenough(valor) {
    return valor >= 40;
};
const a = array1.filter((e) => e > 40);
console.log(a)
// console.log(found);
// expected output: 12
let testes = [
    [100,80,90],
    [100,120,30,50],
    [100,90,30,25]
  ];
  var fila = [];
  
  //Para cada teste(array) dentro de testes
  testes.forEach(filaAtual => {
    // Pegando a quantidade de alunos dentro da fila
    let numeroAlunos = filaAtual.length;
    // A fila atual com as notas dos alunos
    let alunosNotas = filaAtual;
    // Copiamos com o REST e depois fazemos o SORT com uma ARROW FUNCTION, depoia aplicamos o REVERSE
    // (Vale a pena dar uma estudada nos termos em CAPS)
    let ordenada = [...filaAtual].sort((a,b) => a-b).reverse();
  
    let naoTrocou = 0;
    // Contabilizamos os alunos que não trocaram de lugar na fila
    for (let a = 0; a < numeroAlunos; a++) {
      naoTrocou += (alunosNotas[a] === ordenada[a])? 1 : null;
      //alunosNotas[a] == ordenada[a] && naoTrocou++; //Outra forma de atribuir o valor, como no ternário
    }
    // Acrescentamos no final da fila o valor de quantos não trocaram de lugar
    fila.push(naoTrocou);
  }); 
   
  // Printando cada valor separadamente
  for (let trocas of fila) {
    console.log('Quantidade de alunos que não precisaram trocar de lugar:', trocas);
  };